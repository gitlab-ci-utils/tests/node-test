# CHANGELOG

## v2.0.0 (2024-10-27)

### Changed

- BREAKING: Updated project name and path to `node-cjs-test`.

## v1.1.0 (2024-10-20)

### Changed

- Added `test:no-coverage` script to npm package.

## v1.0.1 (2024-07-22)

### Fixed

- Updated `trigger` pipelines to include `lint_prose` and `code_count` jobs to
  avoid false MR data. If these do not run on all pipelines, any data is
  considered a MR change.

## v1.0.0 (2024-06-19)

Initial release
