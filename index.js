'use strict';

/**
 * A function that logs a message to stdout.
 *
 * @param {string} message The message to log.
 */
const logger = (message) => {
    console.log(message);
};

module.exports = logger;
