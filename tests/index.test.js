'use strict';

const log = require('../index');

describe('test something', () => {
    beforeAll(() => {
        jest.spyOn(console, 'log').mockImplementation();
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    it('should log message to stdout', () => {
        expect.assertions(1);
        const message = 'Hello, World!';
        log(message);
        expect(console.log).toHaveBeenCalledWith(message);
    });
});
